import {browser, by, element, ElementArrayFinder} from 'protractor';
import {ElementFinder} from 'protractor/built/element';

export class AppPage {
  navigateTo(path?: string): Promise<unknown> {
    return browser.get(browser.baseUrl + path) as Promise<unknown>;
  }
    getHoursSelectorItems(): ElementArrayFinder {
        return element.all(by.css('[data-test-id="selector-item-id"]'));
    }

    getFirstItemDate(): { day: Promise<string>, hour: Promise<string> } {
        const day = element.all(by.css('[data-test-id="selector-item-day"]')).get(0).getText();
        const hour = element.all(by.css('[data-test-id="selector-item-hour"]')).get(0).getText();

        return {day, hour} as { day: Promise<string>, hour: Promise<string> };
    }
    getCardCityText(): Promise<string> {
        return element(by.css('[data-test-id="weather-card-city"]')).getText() as Promise<string>;
    }

    getCardTemperatureText(): Promise<string>{
        return element(by.css('[data-test-id="weather-card-temp"]')).getText() as Promise<string>;
    }

    getCardWindText(): Promise<string>{
        return element(by.css('[data-test-id="weather-card-wind"]')).getText() as Promise<string>;
    }

    getCardIconSrc(): Promise<string>{
        return element(by.css('[data-test-id="weather-card-icon"]')).getAttribute('src') as Promise<string>;
    }

    getCardDescriptionText(): Promise<string>{
        return element(by.css('[data-test-id="weather-card-description"]')).getText() as Promise<string>;
    }

    getArrowRight(): ElementFinder {
        return element(by.css('[data-test-id="selector-right-arrow"]'));
    }

    getArrowLeft(): ElementFinder {
        return element(by.css('[data-test-id="selector-left-arrow"]'));
    }
}
