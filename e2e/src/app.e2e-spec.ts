import {AppPage} from './app.po';
import {browser, by, element, logging} from 'protractor';
import * as moment from 'moment';
import {DATE_FORMATS} from '../../src/app/common/global.constants';
import {ProtractorExpectedConditions} from 'protractor/built/expectedConditions';

describe('Weather App', () => {
    let page: AppPage;
    let EC: ProtractorExpectedConditions;

    beforeEach(() => {
        page = new AppPage();
        EC = browser.ExpectedConditions;
    });

    it('should navigate by default to the first element in the list', () => {
        page.navigateTo();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + '#/cities/0');
    });

    it('should display the list of hours in the hours selector', () => {
        const nrOfHours = page.getHoursSelectorItems().count();
        const firstItemText = page.getHoursSelectorItems().get(0).getText();
        browser.driver.manage().window().setSize(1500, 900);
        expect(nrOfHours).toEqual(12);
        const currentHour = moment().format(DATE_FORMATS.HOUR);
        const currentTime = moment().format(DATE_FORMATS.WEEK_DAY);
        expect(firstItemText).toContain(currentHour, currentTime);
    });

    it('should have the current class if it is the current hour', () => {
        const itemClass = page.getHoursSelectorItems().get(0).getAttribute('className');
        expect(itemClass).toContain('current');
    });

    it('should set class selected when item clicked', () => {
        const elem = page.getHoursSelectorItems().get(1);
        elem.click();
        const elemClass = elem.getAttribute('className');
        expect(elemClass).toContain('selected');
    });

    it('should show next and previous hour items when clicking left/right arrows', () => {
        browser.driver.manage().window().setSize(1499, 900);
        const currentFirst = page.getFirstItemDate();

        page.getArrowRight().click();
        let updatedFirst = page.getFirstItemDate();
        expect(currentFirst).not.toEqual(updatedFirst.hour);

        page.getArrowLeft().click();
        updatedFirst = page.getFirstItemDate();
        expect(currentFirst.day).toEqual(updatedFirst.day);
        expect(currentFirst.hour).toEqual(updatedFirst.hour);
    });

    it('should navigate to the selected city', () => {
        browser.waitForAngularEnabled(false);
        const secondCitiesItem = element.all(by.css('[data-test-id="cities-item"]')).get(1);
        secondCitiesItem.click();
        browser.wait(EC.urlContains('#/cities/1'), 2000, 'Wait for city data to load.');
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + '#/cities/1');
    });

    it('should display the weather details of the selected item', () => {
        expect(page.getCardIconSrc()).toContain('http://openweathermap.org/img/wn/');
        expect(page.getCardTemperatureText()).toContain('°C');
        expect(page.getCardWindText()).toContain('m/s');
        expect(page.getCardDescriptionText()).toBeTruthy();
        expect(page.getCardCityText()).toEqual('Amsterdam');
    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
