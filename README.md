## Weather

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.4.

## Project description

A web app that displays the weather in different cities. The home page shows a list of cities.
 When the user selects a city, on top is being displayed a list of hours from which the user can select and 
 the weather details for the selected hour. By default the current hour is selected. 
 The user can navigate left and right through the hours list. The current time is marked by an orange border.

## Project structure
    Below you will find each folder with a separate section and its description
    
## components

    These are wrapper components that are not intended to be reused.
The <<home>> component is responsible for loading the data that is being passed to the child components 
(<<cities>>, <<city-weather>>). It is doing so by dispatching actions to retrieve cities list or via the route resolver 
to retrieve city data.  
It is also listening for data in the store and it's passing it on to the <<cities>> child component, it is listening for 
server errors (being sent from interceptor). 

## presentational-components

    These are components that can be reused and have only role to display data.
The <<hour-selector>> receives the hours list, the current time as inputs and it outputs the selected time on the 
click event.
The helper functions are added in a separate service which is tested via unit tests.
The <<toaster>> component, used in the <<app-component>> has a public service that 
can be called to display toast messages anywhere in the app (warning, error, info). 
The <<progress-bar>> works like a vertical spinner and receives one boolean input stating if the animation is active.  
The <<weather-card>> receives weather details as input and displays them.  

## store

The store is more useful in a larger project, not in a small app such as this one. 
I used it for demonstration purpose. It contains the weather state, the actions and 
the effects for async tasks (such as retrieving cities list from the server).

## api 

The ApiService is used for calling backend endpoints and Subjects used globally (error, loading). 
The interceptor is used for pre and post-processing of network requests such as: adding api token key to the params,
handling network errors or request status (loading state).
The Resolver is being used for loading city data on navigation action.

## common

Contains global variables and data models used throughout the app.

## styles

contains the global style variables such as colors, fonts, icon fonts.

## unit tests

I created unit tests for hours-selector service and for city list component.






## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
