import {Actions, Effect, ofType} from '@ngrx/effects';
import * as actions from './weather.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as moment from 'moment';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {of} from 'rxjs';
import {CityListItem} from '../common/models/city-list-item.model';
import {DATE_FORMATS, UNITS} from '../common/global.constants';

@Injectable()
export class WeatherEffects{
  constructor(private actions$: Actions, private http: HttpClient, private api: ApiService) {
  }

  @Effect()
  cities = this.actions$.pipe(
    ofType(actions.GET_CITIES),
    switchMap((data: actions.GetCities) => {
      return this.api.fetchCities(data.payload)
        .pipe(
          map(response => {
            const cities: CityListItem[] = response.list.map(({
                                                               name,
                                                               dt,
                                                               main: {temp},
                                                               wind,
                                                               weather,
                                                               coord,
                                                             }) => ({
              name,
              temp: {val: temp, unit: UNITS.TEMPERATURE},
              wind: {...wind,  unit: UNITS.WIND},
              iconSrc: `http://openweathermap.org/img/wn/${weather[0].icon}@4x.png`,
              date: moment.unix(dt).format(DATE_FORMATS.MONTH_DAY_OF_WEEEK_YEAR), description: weather[0].description,
              coord,
            }));
            return new actions.GetCitiesFulFilled(cities);
          }),
          catchError(() => of(new actions.GetCitiesRejected('An error occurred when retrieving cities cities!')))
        );
    })
  );

}


