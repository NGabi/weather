import * as actions from './weather.actions';
import {CityListItem} from '../common/models/city-list-item.model';

export interface AppState {
  weather: State;
}

export interface State {
      cities: CityListItem[];
}

const initialState: State = {
  cities: [],
};

export function weatherReducer(state = initialState, action: actions.actionTypes): State {
    switch (action.type) {
        case actions.GET_CITIES_FULFILLED:
            return {
                ...state,
                cities: action.payload,
            };
        default:
            return state;
    }
}
