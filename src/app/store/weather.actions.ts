import {Action} from '@ngrx/store';
import {CityListItem} from '../common/models/city-list-item.model';

export const GET_CITIES = 'GET_CITIES';
export const GET_CITIES_FULFILLED = 'GET_CITIES_FULFILLED';
export const GET_CITIES_REJECTED = 'GET_CITIES_REJECTED';

export class GetCities implements Action{
  readonly  type = GET_CITIES;
  constructor(public payload: string){}
}

export class GetCitiesFulFilled implements Action{
  readonly  type = GET_CITIES_FULFILLED;
  constructor(public payload: CityListItem[]){}
}

export class GetCitiesRejected implements Action{
  readonly  type = GET_CITIES_REJECTED;
  constructor(public payload: string){}
}

export type actionTypes = GetCities | GetCitiesFulFilled | GetCitiesRejected ;
