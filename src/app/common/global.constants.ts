export const DATE_FORMATS = {
    DEFAULT: 'MM-DD-YY HH:mm',
    MONTH_DAY_OF_WEEEK_YEAR: 'MMMM d, yy',
    WEEK_DAY: 'ddd',
    HOUR_MINUTES: 'HH:mm',
    HOUR: 'HH',
    MM_DD_YY: 'MM-DD-YY',
};

export const UNITS = {
    TEMPERATURE: '°C',
    WIND: 'm/s',
};

export const URLS = {
    CITIES_WEATHER: 'https://api.openweathermap.org/data/2.5/group',
    CITY_WEATHER: 'https://api.openweathermap.org/data/2.5/onecall',
};

export const LOADING_KEYS = {
    CITIES_WEATHER: 'loadCitiesWeather',
    CITY_WEATHER: 'loadCityWeather',
};
