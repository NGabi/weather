export interface Weather {
  date: string;
  iconSrc: string;
  temp: {val: number, unit: string};
  description: string;
  wind: { speed: number, degree: number, unit: string };
}
