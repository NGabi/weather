export interface CityListItem {
  coord: { lat: number, lon: number };
  name: string;
    date: string;
    iconSrc: string;
    temp: {val: number, unit: string};
    description: string;
    wind: { speed: number, degree: number, unit: string };
}
