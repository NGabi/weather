import {Weather} from './weather.model';

export interface CityWeather {
  current: Weather;
  hourly: Weather[];
}
