import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ApiService} from './api.service';
import {Injectable} from '@angular/core';
import {CityWeather} from '../common/models/city-weather.model';

@Injectable()
export class WeatherDetailsResolver implements Resolve<CityWeather> {
  constructor(private api: ApiService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CityWeather>
    | Promise<CityWeather> | CityWeather {
    return this.api.getCityWeather(route.queryParams.lat, route.queryParams.lon);
  }
}
