import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {GetCities} from './models/get-cities.model';
import {Weather} from '../common/models/weather.model';
import {Observable, Subject} from 'rxjs';
import {ParamsEncoder} from './utils/prams-encoder';
import {GetCityWeatherModel} from './models/get-city-weather.model';
import {map} from 'rxjs/operators';
import * as moment from 'moment';
import {CityWeather} from '../common/models/city-weather.model';
import {DATE_FORMATS, UNITS, URLS} from '../common/global.constants';

@Injectable({providedIn: 'root'})
export class ApiService {
    constructor(private http: HttpClient) {
    }
    error = new Subject<string>();
    isLoading = new Subject<string>();

    fetchCities(citiesIds: string): Observable<GetCities> {
        let params = new HttpParams({encoder: new ParamsEncoder()});
        params = params.append('id', citiesIds);
        params = params.append('units', 'metric');
        return this.http.get<GetCities>(URLS.CITIES_WEATHER, {params});
    }

    getCityWeather(lat: string, lon: string): Observable<CityWeather> {
        let params = new HttpParams();
        params = params.append('lat', lat);
        params = params.append('lon', lon);
        params = params.append('exclude', 'daily,minutely');
        params = params.append('units', 'metric');
        return this.http.get<GetCityWeatherModel>(URLS.CITY_WEATHER, {params})
            .pipe(
                map(response => {
                    const current: Weather = this.mapObject(response.current);
                    const hourly: Weather[] = response.hourly.map(item => this.mapObject(item));
                    return {current, hourly};
                })
            );
    }

    mapObject({wind_deg, weather, dt, temp, wind_speed}): Weather {
        return {
            date: moment.unix(dt).format(DATE_FORMATS.DEFAULT),
            iconSrc: `http://openweathermap.org/img/wn/${weather[0].icon}@4x.png`,
            temp: {val: temp, unit: UNITS.TEMPERATURE},
            description: weather[0].description,
            wind: {speed: wind_speed, degree: wind_deg, unit: UNITS.WIND},
        };
    }
}
