export interface GetCityWeatherModel {
  current: GetCityWeatherItem;
  hourly: GetCityWeatherItem[];
}

interface GetCityWeatherItem{
  dt: number;
  temp: number;
  weather: {description: string, icon: string}[];
  wind_deg: number;
  wind_speed: number;
}
