export interface GetCities {
  list: { name: string,
      dt: number,
      main: { temp: number },
      wind: { speed: number, degree: number },
      weather: {description: string, icon: string}[],
      coord: {lat: number, lon: number},
    }[];
}
