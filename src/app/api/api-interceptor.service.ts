import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ApiService} from './api.service';
import {LOADING_KEYS, URLS} from '../common/global.constants';
import {environment} from '../../environments/environment';

export class ReqInterceptor implements HttpInterceptor {
    constructor(private api: ApiService) {
    }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const params = req.params.append('appid', environment.apiKey);

    const newRequest = req.clone({params});
    return next.handle(newRequest).pipe(tap(event => {
        if (req.url === URLS.CITY_WEATHER) {
            if (event.type === HttpEventType.Sent) {
                this.api.isLoading.next(LOADING_KEYS.CITY_WEATHER);
            }

            if (event.type === HttpEventType.Response) {
                this.api.isLoading.next(null);
            }
        }else if (req.url === URLS.CITIES_WEATHER) {
            if (event.type === HttpEventType.Sent) {
                this.api.isLoading.next(LOADING_KEYS.CITIES_WEATHER);
            }
            if (event.type === HttpEventType.Response) {
                this.api.isLoading.next(null);
            }
        }
    }),
        catchError(() => {
            let errorMsg: string;
            if (req.url === URLS.CITY_WEATHER){
                errorMsg = 'An error has occurred when loading city weather.';
            }else if (req.url === URLS.CITIES_WEATHER){
                errorMsg = 'An error has occurred when loading cities list.';
            }
            this.api.error.next(errorMsg);
            return throwError(errorMsg);
        }));
  }
}
