import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CitiesComponent } from './components/cities/cities.component';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ReqInterceptor} from './api/api-interceptor.service';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import {AppRoutingModule} from './app-routing.module';
import {WeatherDetailsResolver} from './api/weather-detail-resolver.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {StoreModule} from '@ngrx/store';
import {weatherReducer} from './store/weather.reducer';
import {EffectsModule} from '@ngrx/effects';
import {WeatherEffects} from './store/weather.effects';
import {HoursSelectorComponent} from './presentational-components/hours-selector/hours-selector.component';
import {WeatherCardComponent} from './presentational-components/weather-card/weather-card.component';
import {ApiService} from './api/api.service';
import { ProgressBarComponent } from './presentational-components/progress-bar/progress-bar.component';
import { ToasterComponent } from './presentational-components/toaster/toaster.component';

@NgModule({
  declarations: [
    AppComponent,
    CitiesComponent,
    CityWeatherComponent,
    HomeComponent,
    PageNotFoundComponent,
    HoursSelectorComponent,
    WeatherCardComponent,
    ProgressBarComponent,
    ToasterComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NgbModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({weather: weatherReducer}),
    EffectsModule.forRoot([WeatherEffects]),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: ReqInterceptor, multi: true, deps: [ApiService]}, WeatherDetailsResolver],
  bootstrap: [AppComponent],
})
export class AppModule { }
