import {Component, OnDestroy, OnInit} from '@angular/core';
import {LOADING_KEYS} from './common/global.constants';
import {ApiService} from './api/api.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
    title = 'weather';
    isLoading = true;
    isLoadingSubscription: Subscription = null;

    constructor(private api: ApiService) {
    }

    ngOnInit(): void {
        this.isLoadingSubscription = this.api.isLoading.subscribe(loadingItem => {
            this.isLoading = loadingItem === LOADING_KEYS.CITIES_WEATHER;
        });
    }

    ngOnDestroy(): void {
        if (this.isLoadingSubscription) {
            this.isLoadingSubscription.unsubscribe();
        }
    }
}
