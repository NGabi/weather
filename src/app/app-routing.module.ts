import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {CityWeatherComponent} from './components/city-weather/city-weather.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {NgModule} from '@angular/core';
import {WeatherDetailsResolver} from './api/weather-detail-resolver.service';


const routes: Routes = [
  {path: '', redirectTo: '/cities', pathMatch: 'full' },
  {
    path: 'cities', component: HomeComponent, children: [
      {path: ':id/:name', component: CityWeatherComponent, resolve: {weatherDetails: WeatherDetailsResolver}}
    ]
  },
  {path: 'not-found', component: PageNotFoundComponent, data: {message: 'Page not found'}},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, initialNavigation: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
