import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CitiesComponent} from './cities.component';
import {CITIES_MOCK} from './cities.mock';

describe('CitiesComponent', () => {
    let component: CitiesComponent;
    let fixture: ComponentFixture<CitiesComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CitiesComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CitiesComponent);
        component = fixture.componentInstance;
        component.cities = CITIES_MOCK.cities;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain five countries', () => {
        fixture.detectChanges();
        component.cities = CITIES_MOCK.cities;
        const element = fixture.debugElement.nativeElement;
        expect(element.querySelectorAll('[data-test-id=cities-item]').length).toEqual(5);
        expect(component).toBeTruthy();
    });

    it('should emit an event with the index and data of the clicked city', () => {
        spyOn(component, 'onClick');
        const item = fixture.debugElement.nativeElement.querySelector('[data-test-id=cities-item]');
        item.click();
        expect(component.onClick).toHaveBeenCalledWith(0, CITIES_MOCK.city);
    });
});
