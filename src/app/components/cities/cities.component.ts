import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CityListItem} from '../../common/models/city-list-item.model';
import {DATE_FORMATS} from '../../common/global.constants';

@Component({
    selector: 'cities',
    templateUrl: './cities.component.html',
    styleUrls: ['./cities.component.scss']
})
export class CitiesComponent{
    @Output() itemClick: EventEmitter<{ id: number, city: CityListItem }> = new EventEmitter();
    @Input() cities: CityListItem[];
    format = DATE_FORMATS.MONTH_DAY_OF_WEEEK_YEAR;

    onClick(id: number, item: CityListItem): void {
        this.itemClick.emit({id, city: item});
    }
}
