export const CITIES_MOCK = {
    cities: [{
        name: 'London', temp: {val: 15.58, unit: '°C'}, wind: {speed: 0.83, degree: 89, unit: 'm/s'},
        iconSrc: 'http://openweathermap.org/img/wn/50n@4x.png', date: 'October 4, 2020', description: 'mist',
        coord: {lon: -93.25, lat: 35.33}
    }, {
        name: 'Amsterdam', temp: {val: 7.15, unit: '°C'},
        wind: {speed: 1.33, degree: 153, unit: 'm/s'}, iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png', date: 'October 4, 2020',
        description: 'clear sky', coord: {lon: -111.32, lat: 45.76}
    }, {
        name: 'Paris', temp: {val: 17.84, unit: '°C'},
        wind: {speed: 7.7, degree: 220, unit: 'm/s'}, iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png', date: 'October 4, 2020',
        description: 'overcast clouds', coord: {lon: 2.35, lat: 48.86}
    }, {
        name: 'Bucharest', temp: {val: 18.93, unit: '°C'},
        wind: {speed: 5.1, degree: 250, unit: 'm/s'}, iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png', date: 'October 4, 2020',
        description: 'broken clouds', coord: {lon: 26.11, lat: 44.43}
    }, {
        name: 'Vienna', temp: {val: 16.77, unit: '°C'},
        wind: {speed: 5.1, degree: 320, unit: 'm/s'}, iconSrc: 'http://openweathermap.org/img/wn/03d@4x.png', date: 'October 4, 2020',
        description: 'scattered clouds', coord: {lon: 16.37, lat: 48.21}
    }],

    city: {
        name: 'London', temp: {val: 15.58, unit: '°C'}, wind: {speed: 0.83, degree: 89, unit: 'm/s'},
        iconSrc: 'http://openweathermap.org/img/wn/50n@4x.png', date: 'October 4, 2020', description: 'mist',
        coord: {lon: -93.25, lat: 35.33}
    }
};
