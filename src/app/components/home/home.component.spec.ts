import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {Routes} from '@angular/router';
import {MOCK_HOME} from './home.mock';

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;
    let store: MockStore;

    beforeEach(async () => {
        const initialState = {weather: {cities: []}};
        const routes: Routes = MOCK_HOME.routes;
        await TestBed.configureTestingModule({
            declarations: [HomeComponent],
            imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes)],
            providers: [provideMockStore({initialState})]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
