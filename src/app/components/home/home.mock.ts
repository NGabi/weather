import {HomeComponent} from './home.component';
import {CityWeatherComponent} from '../city-weather/city-weather.component';
import {WeatherDetailsResolver} from '../../api/weather-detail-resolver.service';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';

export const mockState =
    {
        weather: {
            cities: [],
            selectedCity: null,
            isLoading: false,
            error: null,
        }
    };

export const MOCK_HOME = {
    routes: [
        {path: '', redirectTo: '/cities', pathMatch: 'full' },
        {
            path: 'cities', component: HomeComponent, children: [
                {path: ':id/:name', component: CityWeatherComponent, resolve: {weatherDetails: WeatherDetailsResolver}}
            ]
        },
        {path: 'not-found', component: PageNotFoundComponent, data: {message: 'Page not found'}},
        {path: '**', redirectTo: '/not-found'}
    ]
};
