import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../../api/api.service';
import {Store} from '@ngrx/store';
import * as actions from '../../store/weather.actions';
import * as weather from '../../store/weather.reducer';
import {ActivatedRoute, Router} from '@angular/router';
import {CityListItem} from '../../common/models/city-list-item.model';
import {LOADING_KEYS} from '../../common/global.constants';
import {AppToastService} from '../../presentational-components/toaster/toaster.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    weatherState: Observable<weather.State>;
    citiesToRetrieve = '4119617,5637141,6455259,683506,2761369';
    isLoading = true;
    error = null;
    subscriptions: Subscription [] = [];

    constructor(private http: HttpClient,
                private api: ApiService,
                private store: Store<weather.AppState>,
                private router: Router,
                private route: ActivatedRoute,
                private toastService: AppToastService) {
    }

    ngOnInit(): void {
        this.weatherState = this.store.select('weather');
        this.store.dispatch(new actions.GetCities(this.citiesToRetrieve));
        this.subscribe = this.api.error.subscribe(errorMessage => {
            this.error = errorMessage;
            this.toastService.show('Error', errorMessage);
        });

        this.subscribe = this.api.isLoading.subscribe(loadingItem => {
            this.isLoading = loadingItem === LOADING_KEYS.CITIES_WEATHER;
        });

        this.subscribe = this.weatherState.subscribe(({cities}) => {
            if (cities.length > 0){
                const {coord: {lat, lon}, name} = cities[0];
                this.router.navigate(['/cities', 0, name],
                    {
                        relativeTo: this.route,
                        queryParams: {lat, lon},
                        fragment: `0`,
                        queryParamsHandling: 'merge',
                    });
            }
        });
    }

    onLoadWeatherDetails(data: { id: number, city: CityListItem }): void {
        const {id, city} = data;
        const {coord: {lat, lon}, name} = city;
        this.router.navigate(['/cities', id, name],
            {
                relativeTo: this.route,
                queryParams: {lat, lon},
                fragment: `${id}`,
                queryParamsHandling: 'merge',
            });
    }

    set subscribe(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

}
