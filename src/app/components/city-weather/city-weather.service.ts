import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class CityWeatherService {
    groupBy(items, key): any {
        return items.reduce(
            (result, item) => ({
                ...result,
                [item[key]]: [
                    ...(result[item[key]] || []),
                    item,
                ],
            }),
            {},
        );
    }
}
