import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {CityWeatherComponent} from './city-weather.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CITY_WEATHER} from './city-weather.mock';
import {ActivatedRoute, Router, Routes} from '@angular/router';
import {MOCK_HOME} from '../home/home.mock';
import {of} from 'rxjs';
import {WeatherDetailsResolver} from '../../api/weather-detail-resolver.service';

describe('CityWeatherComponent', () => {
    let component: CityWeatherComponent;
    let fixture: ComponentFixture<CityWeatherComponent>;
    let route;
    let router: Router;
    let resolver: WeatherDetailsResolver;
    let routes: Routes;
    beforeEach(async () => {
        routes = MOCK_HOME.routes;
        await TestBed.configureTestingModule({
            declarations: [CityWeatherComponent],
            imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes)],
            providers: [WeatherDetailsResolver],
        }).compileComponents();
        resolver = TestBed.inject(WeatherDetailsResolver);
        router = router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
    });

    beforeEach(() => {
        route.data = of({weatherDetails: CITY_WEATHER.weatherDetails});
        fixture = TestBed.createComponent(CityWeatherComponent);
        component = fixture.componentInstance;
        component.id = CITY_WEATHER.id;
        component.cityName = CITY_WEATHER.cityName;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should populate component data on navigation',
        fakeAsync(() => {
            resolver.resolve(route, null);
            router.navigate(['/cities/0/London?lat=35.33&lon=-93.25#0']);
            tick();
            expect(component.current).toEqual(CITY_WEATHER.current);
            expect(component.hourly).toEqual(CITY_WEATHER.hourly);
        }));
});
