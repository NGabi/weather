export const CITY_WEATHER = {

    current: {
        date: '10-11-20 13:41',
        iconSrc: 'http://openweathermap.org/img/wn/50n@4x.png',
        temp: {val: 15.45, unit: '°C'},
        description: 'mist',
        wind: {speed: 1.07, degree: 318, unit: 'm/s'}
    },
    hourly: ['10-11-20 13:00', '10-11-20 14:00', '10-11-20 15:00', '10-11-20 16:00', '10-11-20 17:00', '10-11-20 18:00', '10-11-20 19:00',
        '10-11-20 20:00', '10-11-20 21:00', '10-11-20 22:00', '10-11-20 23:00', '10-12-20 00:00', '10-12-20 01:00', '10-12-20 02:00',
        '10-12-20 03:00', '10-12-20 04:00', '10-12-20 05:00', '10-12-20 06:00', '10-12-20 07:00', '10-12-20 08:00', '10-12-20 09:00',
        '10-12-20 10:00', '10-12-20 11:00', '10-12-20 12:00', '10-12-20 13:00', '10-12-20 14:00', '10-12-20 15:00', '10-12-20 16:00',
        '10-12-20 17:00', '10-12-20 18:00', '10-12-20 19:00', '10-12-20 20:00', '10-12-20 21:00', '10-12-20 22:00', '10-12-20 23:00',
        '10-13-20 00:00', '10-13-20 01:00', '10-13-20 02:00', '10-13-20 03:00', '10-13-20 04:00', '10-13-20 05:00', '10-13-20 06:00',
        '10-13-20 07:00', '10-13-20 08:00', '10-13-20 09:00', '10-13-20 10:00', '10-13-20 11:00', '10-13-20 12:00'],
    mappedData: {
        '10-08-202020 14:00': [{
            date: '10-08-202020 14:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 15.3, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.83, degree: 89, unit: 'm/s'}
        }],
        '10-08-202020 15:00': [{
            date: '10-08-202020 15:00',
            iconSrc: 'http://openweathermap.org/img/wn/03d@4x.png',
            temp: {val: 16.49, unit: '°C'},
            description: 'scattered clouds',
            wind: {speed: 0.43, degree: 173, unit: 'm/s'}
        }],
        '10-08-202020 16:00': [{
            date: '10-08-202020 16:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 18.96, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 0.7, degree: 66, unit: 'm/s'}
        }],
        '10-08-202020 17:00': [{
            date: '10-08-202020 17:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 22.07, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.06, degree: 123, unit: 'm/s'}
        }],
        '10-08-202020 18:00': [{
            date: '10-08-202020 18:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 23.44, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.22, degree: 125, unit: 'm/s'}
        }],
        '10-08-202020 19:00': [{
            date: '10-08-202020 19:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 25.78, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.8, degree: 107, unit: 'm/s'}
        }],
        '10-08-202020 20:00': [{
            date: '10-08-202020 20:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 26.99, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.74, degree: 154, unit: 'm/s'}
        }],
        '10-08-202020 21:00': [{
            date: '10-08-202020 21:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 27.28, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 2.09, degree: 110, unit: 'm/s'}
        }],
        '10-08-202020 22:00': [{
            date: '10-08-202020 22:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 28.03, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.69, degree: 126, unit: 'm/s'}
        }],
        '10-08-202020 23:00': [{
            date: '10-08-202020 23:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 27.79, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.68, degree: 124, unit: 'm/s'}
        }],
        '10-09-202020 00:00': [{
            date: '10-09-202020 00:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 27.21, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 1.58, degree: 115, unit: 'm/s'}
        }],
        '10-09-202020 01:00': [{
            date: '10-09-202020 01:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 24.26, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 2.33, degree: 105, unit: 'm/s'}
        }],
        '10-09-202020 02:00': [{
            date: '10-09-202020 02:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 21.38, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 2, degree: 105, unit: 'm/s'}
        }],
        '10-09-202020 03:00': [{
            date: '10-09-202020 03:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 20.2, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.81, degree: 112, unit: 'm/s'}
        }],
        '10-09-202020 04:00': [{
            date: '10-09-202020 04:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 19.33, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.5, degree: 115, unit: 'm/s'}
        }],
        '10-09-202020 05:00': [{
            date: '10-09-202020 05:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 18.65, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.41, degree: 117, unit: 'm/s'}
        }],
        '10-09-202020 06:00': [{
            date: '10-09-202020 06:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 18.05, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.18, degree: 83, unit: 'm/s'}
        }],
        '10-09-202020 07:00': [{
            date: '10-09-202020 07:00',
            iconSrc: 'http://openweathermap.org/img/wn/02n@4x.png',
            temp: {val: 17.79, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 1.16, degree: 88, unit: 'm/s'}
        }],
        '10-09-202020 08:00': [{
            date: '10-09-202020 08:00',
            iconSrc: 'http://openweathermap.org/img/wn/02n@4x.png',
            temp: {val: 17.64, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 1.06, degree: 76, unit: 'm/s'}
        }],
        '10-09-202020 09:00': [{
            date: '10-09-202020 09:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.6, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.18, degree: 86, unit: 'm/s'}
        }],
        '10-09-202020 10:00': [{
            date: '10-09-202020 10:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.46, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 1.16, degree: 80, unit: 'm/s'}
        }],
        '10-09-202020 11:00': [{
            date: '10-09-202020 11:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.77, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.19, degree: 95, unit: 'm/s'}
        }],
        '10-09-202020 12:00': [{
            date: '10-09-202020 12:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.97, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.06, degree: 97, unit: 'm/s'}
        }],
        '10-09-202020 13:00': [{
            date: '10-09-202020 13:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.65, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.13, degree: 73, unit: 'm/s'}
        }],
        '10-09-202020 14:00': [{
            date: '10-09-202020 14:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.87, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 0.83, degree: 87, unit: 'm/s'}
        }],
        '10-09-202020 15:00': [{
            date: '10-09-202020 15:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 18.76, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.09, degree: 82, unit: 'm/s'}
        }],
        '10-09-202020 16:00': [{
            date: '10-09-202020 16:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 20.35, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.32, degree: 96, unit: 'm/s'}
        }],
        '10-09-202020 17:00': [{
            date: '10-09-202020 17:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 22.15, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 2.04, degree: 127, unit: 'm/s'}
        }],
        '10-09-202020 18:00': [{
            date: '10-09-202020 18:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 23.96, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 3.17, degree: 136, unit: 'm/s'}
        }],
        '10-09-202020 19:00': [{
            date: '10-09-202020 19:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 24.96, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 3.61, degree: 139, unit: 'm/s'}
        }],
        '10-09-202020 20:00': [{
            date: '10-09-202020 20:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 25.51, unit: '°C'},
            description: 'broken clouds',
            wind: {speed: 3.29, degree: 138, unit: 'm/s'}
        }],
        '10-09-202020 21:00': [{
            date: '10-09-202020 21:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 26.38, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 3.43, degree: 143, unit: 'm/s'}
        }],
        '10-09-202020 22:00': [{
            date: '10-09-202020 22:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 25.61, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 3.51, degree: 150, unit: 'm/s'}
        }],
        '10-09-202020 23:00': [{
            date: '10-09-202020 23:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 24.54, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 3.77, degree: 152, unit: 'm/s'}
        }],
        '10-10-202020 00:00': [{
            date: '10-10-202020 00:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 23.6, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 3.45, degree: 149, unit: 'm/s'}
        }],
        '10-10-202020 01:00': [{
            date: '10-10-202020 01:00',
            iconSrc: 'http://openweathermap.org/img/wn/04d@4x.png',
            temp: {val: 22.29, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 2.08, degree: 132, unit: 'm/s'}
        }],
        '10-10-202020 02:00': [{
            date: '10-10-202020 02:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 20.42, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.98, degree: 113, unit: 'm/s'}
        }],
        '10-10-202020 03:00': [{
            date: '10-10-202020 03:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 19.4, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 2.05, degree: 124, unit: 'm/s'}
        }],
        '10-10-202020 04:00': [{
            date: '10-10-202020 04:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.83, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 2, degree: 102, unit: 'm/s'}
        }],
        '10-10-202020 05:00': [{
            date: '10-10-202020 05:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.45, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.68, degree: 96, unit: 'm/s'}
        }],
        '10-10-202020 06:00': [{
            date: '10-10-202020 06:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.12, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.24, degree: 51, unit: 'm/s'}
        }],
        '10-10-202020 07:00': [{
            date: '10-10-202020 07:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.45, degree: 49, unit: 'm/s'}
        }],
        '10-10-202020 08:00': [{
            date: '10-10-202020 08:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.77, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.44, degree: 62, unit: 'm/s'}
        }],
        '10-10-202020 09:00': [{
            date: '10-10-202020 09:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.14, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.93, degree: 49, unit: 'm/s'}
        }],
        '10-10-202020 10:00': [{
            date: '10-10-202020 10:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.94, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.84, degree: 24, unit: 'm/s'}
        }],
        '10-10-202020 11:00': [{
            date: '10-10-202020 11:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.81, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.54, degree: 15, unit: 'm/s'}
        }],
        '10-10-202020 12:00': [{
            date: '10-10-202020 12:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 17.95, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.76, degree: 14, unit: 'm/s'}
        }],
        '10-10-202020 13:00': [{
            date: '10-10-202020 13:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 18.26, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.82, degree: 6, unit: 'm/s'}
        }]
    },
    cityName: 'London',
    id: 0,
    weatherDetails: {
        current: {
            date: '10-11-20 13:41',
            iconSrc: 'http://openweathermap.org/img/wn/50n@4x.png',
            temp: {val: 15.45, unit: '°C'},
            description: 'mist',
            wind: {speed: 1.07, degree: 318, unit: 'm/s'}
        },
        hourly: [{
            date: '10-11-20 13:00',
            iconSrc: 'http://openweathermap.org/img/wn/04n@4x.png',
            temp: {val: 15.45, unit: '°C'},
            description: 'overcast clouds',
            wind: {speed: 1.07, degree: 318, unit: 'm/s'}
        }, {
            date: '10-11-20 14:00',
            iconSrc: 'http://openweathermap.org/img/wn/03n@4x.png',
            temp: {val: 16.32, unit: '°C'},
            description: 'scattered clouds',
            wind: {speed: 0.83, degree: 29, unit: 'm/s'}
        }, {
            date: '10-11-20 15:00',
            iconSrc: 'http://openweathermap.org/img/wn/02d@4x.png',
            temp: {val: 17.66, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 0.73, degree: 23, unit: 'm/s'}
        }, {
            date: '10-11-20 16:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 20.73, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.77, degree: 349, unit: 'm/s'}
        }, {
            date: '10-11-20 17:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 24.07, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.6, degree: 339, unit: 'm/s'}
        }, {
            date: '10-11-20 18:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 26.81, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.16, degree: 176, unit: 'm/s'}
        }, {
            date: '10-11-20 19:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 28.21, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.06, degree: 182, unit: 'm/s'}
        }, {
            date: '10-11-20 20:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 28.75, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.49, degree: 167, unit: 'm/s'}
        }, {
            date: '10-11-20 21:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 29.18, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.81, degree: 165, unit: 'm/s'}
        }, {
            date: '10-11-20 22:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 29.23, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 2.22, degree: 159, unit: 'm/s'}
        }, {
            date: '10-11-20 23:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 29.25, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 2.13, degree: 151, unit: 'm/s'}
        }, {
            date: '10-12-20 00:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 28.88, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.88, degree: 156, unit: 'm/s'}
        }, {
            date: '10-12-20 01:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 26.54, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.51, degree: 142, unit: 'm/s'}
        }, {
            date: '10-12-20 02:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 23.66, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.85, degree: 142, unit: 'm/s'}
        }, {
            date: '10-12-20 03:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 22.1, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 2.01, degree: 126, unit: 'm/s'}
        }, {
            date: '10-12-20 04:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 20.86, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.85, degree: 125, unit: 'm/s'}
        }, {
            date: '10-12-20 05:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 20.27, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.99, degree: 119, unit: 'm/s'}
        }, {
            date: '10-12-20 06:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 19.9, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.93, degree: 121, unit: 'm/s'}
        }, {
            date: '10-12-20 07:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 19.66, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.86, degree: 120, unit: 'm/s'}
        }, {
            date: '10-12-20 08:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 19.46, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.63, degree: 125, unit: 'm/s'}
        }, {
            date: '10-12-20 09:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 19.34, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.72, degree: 123, unit: 'm/s'}
        }, {
            date: '10-12-20 10:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 19.23, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.98, degree: 117, unit: 'm/s'}
        }, {
            date: '10-12-20 11:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 18.97, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.56, degree: 111, unit: 'm/s'}
        }, {
            date: '10-12-20 12:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 18.7, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.92, degree: 98, unit: 'm/s'}
        }, {
            date: '10-12-20 13:00',
            iconSrc: 'http://openweathermap.org/img/wn/02n@4x.png',
            temp: {val: 18.81, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 1.36, degree: 145, unit: 'm/s'}
        }, {
            date: '10-12-20 14:00',
            iconSrc: 'http://openweathermap.org/img/wn/02n@4x.png',
            temp: {val: 18.57, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 0.36, degree: 267, unit: 'm/s'}
        }, {
            date: '10-12-20 15:00',
            iconSrc: 'http://openweathermap.org/img/wn/02d@4x.png',
            temp: {val: 19.27, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 1.55, degree: 315, unit: 'm/s'}
        }, {
            date: '10-12-20 16:00',
            iconSrc: 'http://openweathermap.org/img/wn/02d@4x.png',
            temp: {val: 21.37, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 2.9, degree: 319, unit: 'm/s'}
        }, {
            date: '10-12-20 17:00',
            iconSrc: 'http://openweathermap.org/img/wn/03d@4x.png',
            temp: {val: 22.81, unit: '°C'},
            description: 'scattered clouds',
            wind: {speed: 4.05, degree: 342, unit: 'm/s'}
        }, {
            date: '10-12-20 18:00',
            iconSrc: 'http://openweathermap.org/img/wn/02d@4x.png',
            temp: {val: 23.62, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 4.88, degree: 350, unit: 'm/s'}
        }, {
            date: '10-12-20 19:00',
            iconSrc: 'http://openweathermap.org/img/wn/02d@4x.png',
            temp: {val: 24.19, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 4.13, degree: 350, unit: 'm/s'}
        }, {
            date: '10-12-20 20:00',
            iconSrc: 'http://openweathermap.org/img/wn/02d@4x.png',
            temp: {val: 25.47, unit: '°C'},
            description: 'few clouds',
            wind: {speed: 3.87, degree: 348, unit: 'm/s'}
        }, {
            date: '10-12-20 21:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 26.22, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 3.75, degree: 352, unit: 'm/s'}
        }, {
            date: '10-12-20 22:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 26.34, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 3.88, degree: 356, unit: 'm/s'}
        }, {
            date: '10-12-20 23:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 25.77, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 3.61, degree: 1, unit: 'm/s'}
        }, {
            date: '10-13-20 00:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 24.63, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 2.55, degree: 1, unit: 'm/s'}
        }, {
            date: '10-13-20 01:00',
            iconSrc: 'http://openweathermap.org/img/wn/01d@4x.png',
            temp: {val: 21.31, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 2.14, degree: 6, unit: 'm/s'}
        }, {
            date: '10-13-20 02:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 18.09, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.73, degree: 8, unit: 'm/s'}
        }, {
            date: '10-13-20 03:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 16.71, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.71, degree: 2, unit: 'm/s'}
        }, {
            date: '10-13-20 04:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 15.57, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.62, degree: 13, unit: 'm/s'}
        }, {
            date: '10-13-20 05:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 14.56, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.41, degree: 23, unit: 'm/s'}
        }, {
            date: '10-13-20 06:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 13.83, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.47, degree: 358, unit: 'm/s'}
        }, {
            date: '10-13-20 07:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 13.14, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.54, degree: 342, unit: 'm/s'}
        }, {
            date: '10-13-20 08:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 12.51, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.36, degree: 347, unit: 'm/s'}
        }, {
            date: '10-13-20 09:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 12.05, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 1.13, degree: 342, unit: 'm/s'}
        }, {
            date: '10-13-20 10:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 11.63, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.77, degree: 5, unit: 'm/s'}
        }, {
            date: '10-13-20 11:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 11.35, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.62, degree: 43, unit: 'm/s'}
        }, {
            date: '10-13-20 12:00',
            iconSrc: 'http://openweathermap.org/img/wn/01n@4x.png',
            temp: {val: 11.02, unit: '°C'},
            description: 'clear sky',
            wind: {speed: 0.46, degree: 51, unit: 'm/s'}
        }]
    },
};
