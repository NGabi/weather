import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Params} from '@angular/router';
import {Weather} from '../../common/models/weather.model';
import {ApiService} from '../../api/api.service';
import {Subscription} from 'rxjs';
import {LOADING_KEYS} from '../../common/global.constants';
import {CityWeatherService} from './city-weather.service';

@Component({
    selector: 'app-weather-details',
    templateUrl: './city-weather.component.html',
    styleUrls: ['./city-weather.component.css']
})
export class CityWeatherComponent implements OnInit, OnDestroy {
    current: Weather;
    hourly: string[];
    mappedHours;
    cityName: string;
    id: number;
    subscriptions: Subscription [] = [];
    error: string;
    isLoading = false;

    constructor(private route: ActivatedRoute,
                private api: ApiService,
                private cityService: CityWeatherService) {
    }

    ngOnInit(): void {
        this.route.data.subscribe(
            (data: Data) => {
                const {current, hourly} = data.weatherDetails;
                this.current = current;
                this.hourly = hourly.map(item => item.date);
                this.mappedHours = this.cityService.groupBy(hourly, 'date');
            }
        );

        this.route.params.subscribe((params: Params) => {
            this.cityName = params.name;
            this.id = +params.id;
        });

        this.subscribe = this.api.isLoading.subscribe(loadingItem => {
            this.isLoading = loadingItem === LOADING_KEYS.CITY_WEATHER;
        });
    }

    onSelect(date: string): void {
        this.current = this.mappedHours[date][0];
    }

    set subscribe(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}
