import {Component, Input, OnInit} from '@angular/core';
import {AppToastService} from './toaster.service';

@Component({
    selector: 'app-toaster',
    templateUrl: './toaster.component.html',
    styleUrls: ['./toaster.component.scss']
})
export class ToasterComponent implements OnInit {
    constructor(public toastService: AppToastService) {
    }

    @Input() type: 'danger' | 'warning' | 'notification';

    ngOnInit(): void {
    }

}
