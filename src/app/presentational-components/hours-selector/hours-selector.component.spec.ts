import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HoursSelectorComponent} from './hours-selector.component';
import {HOURS_SELECTOR_MOCK} from './hour-selector.mock';

describe('HoursSelectorComponent', () => {
    let component: HoursSelectorComponent;
    let fixture: ComponentFixture<HoursSelectorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [HoursSelectorComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(HoursSelectorComponent);
        component = fixture.componentInstance;
        component.hours = HOURS_SELECTOR_MOCK.hours;
        component.interval = HOURS_SELECTOR_MOCK.interval;
        component.currentHour =  HOURS_SELECTOR_MOCK.currentHour;
        fixture.detectChanges();
    });

});
