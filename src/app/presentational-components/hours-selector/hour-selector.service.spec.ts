import {HourSelectorService} from './hour-selector.service';

describe('HourSelectorService', () => {
    let service: HourSelectorService;

    beforeEach(() => {
        service = new HourSelectorService();
    });

    afterEach(() => {
        service = null;

    });

    describe('getListItemClass', () => {
        it('should return odd class', () => {
            const {index, date, currentDate, selectedDate} = {
                index: 1,
                date: '10-08-202020 14:00',
                currentDate: '10-08-202020 13:00',
                selectedDate: '10-08-202020 15:00'
            };
            expect(service.getListItemClass(index, date, currentDate, selectedDate)).toEqual('odd');
        });

        it('should return even class', () => {
            const {index, date, currentDate, selectedDate} = {
                index: 2,
                date: '10-08-202020 14:00',
                currentDate: '10-08-202020 13:00',
                selectedDate: '10-08-202020 15:00'
            };
            expect(service.getListItemClass(index, date, currentDate, selectedDate)).toEqual('even');
        });

        it('should return selected class', () => {
            const {index, date, currentDate, selectedDate} = {
                index: 1,
                date: '10-08-202020 14:00',
                currentDate: '10-08-202020 14:00',
                selectedDate: '10-08-202020 13:00'
            };
            expect(service.getListItemClass(index, date, currentDate, selectedDate)).toEqual('current');
        });

        it('should return selected class', () => {
            const {index, date, currentDate, selectedDate} = {
                index: 1,
                date: '10-08-202020 14:00',
                currentDate: '10-08-202020 13:00',
                selectedDate: '10-08-202020 14:00'
            };
            expect(service.getListItemClass(index, date, currentDate, selectedDate)).toEqual('selected');
        });
    });

    describe('getIntervalSize', () => {
        it('should return 12 for screen width < 1500px and items number 48 ', () => {
            expect(service.getIntervalSize(1499, 48)).toEqual(12);
        });

        it('should return 10 for screen width < 1500px and items number 10 ', () => {
            expect(service.getIntervalSize(1499, 10)).toEqual(10);
        });

        it('should return 20 for screen width < 1800px and items number 48 ', () => {
            expect(service.getIntervalSize(1800, 48)).toEqual(20);
        });
    });

    describe('getCurrentDay', () => {
        it('should return the current hour', () => {
            const mockHours = ['09-08-202020 14:00', '10-08-202020 12:00', '10-08-202020 14:00'];
            expect(service.getCurrentDay(mockHours, '10-08-202020 14:29')).toEqual(
                {index: 2, date: '10-08-202020 14:00'});
        });
    });

    describe('getInterval', () => {
        it('should return the items in  the interval, start index and end index ', () => {
            const mockHours = ['10-08-202020 12:00', '10-08-202020 13:00', '10-08-202020 14:00', '10-08-202020 12:00', '10-08-202020 13:00', '10-08-202020 14:00'];
            expect(service.getInterval(mockHours, 3, 6)).toEqual({start: 0, end: 6, interval: mockHours});
        });
    });
});
