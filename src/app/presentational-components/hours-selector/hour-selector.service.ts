import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {DATE_FORMATS} from '../../common/global.constants';

@Injectable({providedIn: 'root'})
export class HourSelectorService {
    getListItemClass(index: number, date: string, currentDate: string, selectedDate: string): string {
        if (date === currentDate) {
            return 'current';
        } else if (selectedDate === date) {
            return 'selected';
        } else {
            return (index % 2 === 0 ? 'even' : 'odd');
        }
    }

    getIntervalSize(currentSize: number, max: number): number {
        // calculate number of item displayed based on window size
        if (currentSize < 576 && max > 4) {
            return 4;
        } else if (currentSize < 768 && max >= 6) {
            return 6;
        } else if (currentSize < 900 && max >= 8) {
            return 8;
        } else if (currentSize < 1500 && max >= 12) {
            return 12;
        } else if (currentSize < 1800 && max >= 18) {
            return 18;
        }

        return max < 20 ? max : 20;
    }

    getCurrentDay(hours: string[], currentHour: string): { index: number, date: string } {
        const index = hours.findIndex(hour => {
            return moment(currentHour, DATE_FORMATS.DEFAULT).format(DATE_FORMATS.HOUR)
                === moment(hour, DATE_FORMATS.DEFAULT).format(DATE_FORMATS.HOUR)
                && moment(currentHour, DATE_FORMATS.DEFAULT).format(DATE_FORMATS.MM_DD_YY)
                === moment(hour, DATE_FORMATS.DEFAULT).format(DATE_FORMATS.MM_DD_YY);
        });
        return ({index, date: hours[index]});
    }

    getInterval(hours: string[], currentIndex: number, intervalSize: number): { start: number, end: number, interval: string[] } {
        // initiate the items that are visible per click left/right,
        const start = Math.floor(currentIndex / intervalSize) * intervalSize;
        const end = start + intervalSize;
        const interval = hours.slice(start, end);
        return {start, end, interval};
    }
}
