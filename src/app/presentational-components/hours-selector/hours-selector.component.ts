import {Component, EventEmitter, HostListener, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';

import {Subject, Subscription} from 'rxjs';
import {HourSelectorService} from './hour-selector.service';
import {DATE_FORMATS} from '../../common/global.constants';
import * as moment from 'moment';


@Component({
    selector: 'hours-selector',
    templateUrl: './hours-selector.component.html',
    styleUrls: ['./hours-selector.component.scss']
})
export class HoursSelectorComponent implements OnInit, OnDestroy, OnChanges {
    @Output() itemSelect: EventEmitter<string> = new EventEmitter();
    @Input() hours: string[];
    @Input() currentHour: string;
    subscriptions: Subscription [] = [];
    intervalChanged: Subject<any> = new Subject<any>();
    interval: string[];
    start = 0;
    end: number;
    intervalSize: number;
    innerWidth;
    rightBtnActive = true;
    leftBtnActive = true;
    selected: { index: number, date: string };
    current: { index: number, date: string };
    FORMATS = DATE_FORMATS;

    constructor(private selectorSvc: HourSelectorService) {
    }

    ngOnInit(): void {
        this.initializeUI();
        this.subscribe = this.intervalChanged.subscribe(newInterval => {
            this.interval = newInterval;
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        const oldHours = changes.hours ? changes.hours.previousValue : this.hours;
        const isChanged = oldHours !== this.hours;
        if (isChanged) {
            this.initializeUI();
        }
    }

    initializeUI(): void {
        this.current = this.selectorSvc.getCurrentDay(this.hours, this.currentHour);
        this.selected = null;
        this.intervalSize = this.selectorSvc.getIntervalSize( window.innerWidth, this.hours.length);
        this.innerWidth = window.innerWidth;
        const {interval, start, end} = this.selectorSvc.getInterval(this.hours, this.current.index, this.intervalSize);
        this.interval = interval;
        this.start = start;
        this.end = end;
        this.leftBtnActive = start > 0;
        this.rightBtnActive = end < this.hours.length;
    }

    renderInterval(): void {
        // recalculate interval when window width is changing
        this.end = this.start + this.intervalSize;
        this.interval = this.hours.slice(this.start, this.end);
    }

    @HostListener('window:resize', ['$event'])
    onResize(): void {
        // calculate number of item displayed based on window size
        const newRange = this.selectorSvc.getIntervalSize(window.innerWidth, this.hours.length);
        if (this.intervalSize !== newRange) {
            this.intervalSize = newRange;
            this.renderInterval();
        }
        this.innerWidth = window.innerWidth;
    }

    onSelect(date: string, index: number): void {
        this.itemSelect.emit(date);
        this.selected = {date, index};
    }

    navigateLeft(): void {
        const newStart = this.start - this.intervalSize;
        const newEnd = this.end - this.intervalSize;

        if (newStart >= 0) {
            this.start = newStart;
            this.end = newEnd;
            this.rightBtnActive = true;
        } else { // handle left limit case
            this.start = 0;
            this.end = this.start + this.intervalSize;
            this.leftBtnActive = false;
        }
        this.intervalChanged.next(this.hours.slice(this.start, this.end));
    }

    navigateRight(): void {
        const newStart = this.start + this.intervalSize;
        const newEnd = this.end + this.intervalSize;

        if (newEnd <= this.hours.length) {
            this.start = newStart;
            this.end = newEnd;
            this.leftBtnActive = true;
        } else { // handle right limit case
            this.start = this.hours.length - this.intervalSize;
            this.end = this.hours.length;
            this.rightBtnActive = false;
        }

        this.intervalChanged.next(this.hours.slice(this.start, this.end));
    }

    listItemClass(index, date): string {
        const selectedDate = this.selected ? this.selected.date : '';
        return this.selectorSvc.getListItemClass(index, date, this.current.date, selectedDate);
    }

    calculateClass(itemName): string {
        switch (itemName) {
            case 'rightBtnActive':
                return this.rightBtnActive ? 'enabled' : '';
            case 'leftBtnActive':
                return this.leftBtnActive ? 'enabled' : '';
            default:
                return '';
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    getWeekDay(date: string): string{
        return moment(date, DATE_FORMATS.DEFAULT).format(DATE_FORMATS.WEEK_DAY);
    }
    getHour(date: string): string{
        return moment(date, DATE_FORMATS.DEFAULT).format(DATE_FORMATS.HOUR_MINUTES);
    }
    set subscribe(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }
}
