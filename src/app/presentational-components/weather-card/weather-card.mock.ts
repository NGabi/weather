
export const WEATHER_CARD_INPUTS_MOCK = {
    city: 'London',
    icon: 'http://openweathermap.org/img/wn/01n@4x.png',
    description: 'clear sky',
    date: 'Saturday, October 10th 2020, 10:00 pm',
    wind: {speed: 12, degree: 2, unit: 'm/s'},
    temperature: {val: 20, unit: '°C'},
};
