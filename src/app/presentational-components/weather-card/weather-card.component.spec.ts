import { ComponentFixture, TestBed } from '@angular/core/testing';
import {WeatherCardComponent} from './weather-card.component';
import {WEATHER_CARD_INPUTS_MOCK} from './weather-card.mock';

describe('WeatherCardComponent', () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    component.city = WEATHER_CARD_INPUTS_MOCK.city;
    component.temperature = WEATHER_CARD_INPUTS_MOCK.temperature;
    component.icon = WEATHER_CARD_INPUTS_MOCK.icon;
    component.date = WEATHER_CARD_INPUTS_MOCK.date;
    component.description = WEATHER_CARD_INPUTS_MOCK.description;
    component.wind = WEATHER_CARD_INPUTS_MOCK.wind;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
