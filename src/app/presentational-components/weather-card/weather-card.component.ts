import {Component, Input} from '@angular/core';
import * as moment from 'moment';
import {DATE_FORMATS} from '../../common/global.constants';
@Component({
  selector: 'weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent{
  @Input() city: string;
  @Input() icon: string;
  @Input() description: string;
  @Input() date: string;
  @Input() wind: {speed: number, degree: number, unit: string};
  @Input() temperature: {val: number, unit: string};

    getDate(date: string): string {
      return  moment(date, DATE_FORMATS.DEFAULT).format('dddd, MMMM Do YYYY, h:mm a');
  }
}
