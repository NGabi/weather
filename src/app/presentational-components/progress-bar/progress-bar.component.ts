import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
    selector: 'app-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit, OnChanges {
    @Input() showProgress = false;
    width: number;
    id: number;
    elem;
    stopped;
    constructor() {

    }

    ngOnInit(): void {
        this.elem = document.getElementById('bar');
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.showProgress.currentValue === true){
            this.move();
        } else {
            this.stop();
        }
    }

    move(): void {
        this.width = 0;
        this.stopped = false;
        setInterval(() => this.frame(), 10);
    }

    frame(): void {
        if (this.width >= 100 || this.stopped === true) {
            clearInterval(this.id);
            // resetting the width makes the animation repeat indefinitely
            this.width = 0;
            this.elem.style.width = this.width + '%';
        } else {
            this.width++;
            this.elem.style.width = this.width + '%';
        }
    }

    stop(): void{
        this.stopped = true;
    }
}
